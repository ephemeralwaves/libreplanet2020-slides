# Field Notes from a Privacy Activist, from China to the US

### Framing the issues

---

## Outline



Talk idea

Lessons learned from the Us/China Internet landscapes and ppl in it

Title
Field Notes from a Digital Rights Activist,
Lessons learned in the US and China

Abstract/Description
In their work doing privacy workshops, art exhibitions and FOSS advocacy, Giselle discusses community building strategies and heuristics in two seemingly different environments, the US and China. They will examine cultural perspectives and reflect on those experiences to better understanding ourselves and how to communicate to grow the movement.


This talk is a reflection of a decade of promoting digital rights in both the China and the US. It will examine cultural attitudes learned in the field and community building strategies and heuristics.



Community engagement strategies and framings

Background

- Giselle is an artist, self-taught programmer, and Outreachy alumna. They like to explore the intersections of art, science, and technology and have been exclusively using FOSS in their practice for several years.  They have worked in tech and at an import/export multinational company which lead them to travel back and forth between China and the US. They are a member of the Pittsburgh Restore the Fourth chapter, The Big Idea and Stranger Company Art Collective. In their spare time they volunteer for various FOSS projects such as Mediawiki and write songs about PGP. They have exhibited at a number of private and State galleries in China as well as in the US.

https://gisellej.com/exhibitions/
https://thinkcolorful.org

What I learned:

Strategy 1: Pay attention to current events, take opportunities to teach ppl IN CONTEXT, those people are most willing to listen and potentially change
Examples:
LGBTQ focus on Doxxing(define the term) workshop, hosted in a safer space, made sure it's a dialogue and not a lecture. No questions were regarded as stupid. hacking for humanity...broaden the pov
SWOP facial recognition protest - plants walk put of hackathon and instigate while those who protest, protest from outside.
--Initiated a diverse approach since ppl had different comfort levels as to how they would like to participate.
Summit against Racism -panel plus breakout groups. color of surveillance: shotspotter, surveillance landscapes -safety being abused

yes im more controlled, but at least i have safety

Strategy 1.1
 Emphasize harm reduction and foster a culture of consent.
 --picture policy//or ask before pictures, like in social relation asking for hugs
 --figure out safer ways to communicate, Signal works well in both countries, use it!
 --BI data infrastructure- FOSS libresass, right to change service providers(http://libresaas.org/)
 -- share a social media account (BI facebook, vpns) promote solidarity
--Working with other various groups to provide free consulting as to threat modeling, to think about data and harm reduction methods applied to data security.

Problem: But what if you're in an environment where you can't explicitly talk about these things?
Strategy 1.2: Throw a party! or host an event that will appeal to a broader audience and make friends!
Know your audience and the environment they are in.
Used art as a way to talk about copyright and free software at a state gallery. They seemed keen on protecting copyright of the artists so it was surprising for them to learn how I license my work. Interesting how they tried to cater to the what they thought I, as an American, would want in terms of copyright 'protection'.
Gave a talk about how i used FOSS to create the art and framed the licensing in terms of software for the people, by the people.  
Copyleft.gallry https://web.archive.org/web/20180107124017/https://copyleft.gallery/
Talk to ppl, figure out what matters to them. Some might just open up and ask for assistance.
And if they do ask...
Be prepared to help!
 China -better to pre download software and share when on the inside. Knowledge of what VPN's work or at least are current with gfw trends (12vpn, expressvpn, tor bridge).
 Some Thoughts about the GFW
 --protectionist -convenience of Chinese websites boosts local tech firms allowing them to build up industry and not be colonized by foreign companies
 --people say they feel safer/ think their companies are superior, things are horrible outside
 --ignorance is bliss state

 Strategy 1.3
 Framing
 --China vs US tech practices
 It's important to understand both
   eff articles China censors and is increasing global reach. US has been doing this for years but no state  mandated client-side filtering and surveillance (william bar is pushing for this though in current cryptowars https://cyberlaw.stanford.edu/blog/2019/10/william-barr-and-winnie-pooh).(https://www.eff.org/deeplinks/2019/10/chinas-global-reach-surveillance-and-censorship-beyond-great-firewall)
   voting, the .02% nomination filter in HK (govener) and US (superPAC funds from 57k ppl who matter, 1/2 from 400 families), domocracy responsive to [rich, china] (lessig https://youtu.be/PJy8vTu66tE?t=479)
   social credit score (ccc talk https://youtu.be/i8sv-JIJ3pc?t=438) ... chinese credit score systems which are visible to us as compared to the market driven social credit scores we are immersed in.
   Living under propaganda machines. one state the other  corp. one 1984 one BNW
https://www.youtube.com/watch?v=onZ1U4jKJdk - Societies of Control

Strategy 1.4
 Talk about broader economic issues and alternative ownership schemes and examples of each. Barcelona.
 Solidarity and collective action
 Worker Cooperatives   https://www.techworker.coop/


How the state and/or corps make it hard:

great firewall-purposely making it difficult
convenience
captive audiences/data/media ( collective member solution to share account )
accessibility -add more tests?

Be prepared to acknowledge pros and cons and show limits of the system.
 and emphasize harm reduction and foster a culture of consent. Talk about broader economic issues and alternative ownership schemes and examples of each. Evgeny Morozov: Breaking up tech firms is a false economy and redesigning a decentralized public platform for data is the only way to short circuit growing asymmetries of power which hold back human progress (but suit nation states)?


PGP song

Framing....China vs US tech pratices
eff articles, voting (lessig), social credit score (ccc talk)
living under propaganda machines. one state the other  corp. one 1984 one BNW

non-market social coordination(check art book)https://twitter.com/evgenymorozov/status/1179717746256437251

What didn't work so great
Teach-ins


Outline:
Field Notes from a Digital Rights activists
* My background
 - Credits
* Strategies/heuristics
  1.Teach in Context
    a.Workshops
    b.Protests
    c.Conferences
  2.Harm Reduction and Culture of consent
    a.Privacy Policies at events
    b.libreSASS
    c.Sharing of accounts
  3.Social/Artistic events
    a.Chinese state gallery
    b.Chinese private galleries
    c.US galleries
  4.Framing and Empathy - Social Psychology
    a.Censorship
    b.Democracy
    c.Social Credit Score
  5.Economics
    a.Worker Co-ops  
    b.Data Ownership
  * Works Cited
