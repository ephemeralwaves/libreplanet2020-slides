# reveal.js Presentation

The slides used for the Libreplanet 2020 presentation. Just download and open the index.html file in a browser.


## License

revel.js is MIT licensed

CC-SA for content
